﻿namespace BT1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbOptionView = new System.Windows.Forms.ComboBox();
            this.cbDrive = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.wbDrive = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbOptionView);
            this.panel1.Controls.Add(this.cbDrive);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 60);
            this.panel1.TabIndex = 0;
            // 
            // cbOptionView
            // 
            this.cbOptionView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOptionView.FormattingEnabled = true;
            this.cbOptionView.Items.AddRange(new object[] {
            "Details",
            "LargeIcon",
            "List",
            "SmallIcon",
            "Tile"});
            this.cbOptionView.Location = new System.Drawing.Point(195, 12);
            this.cbOptionView.Name = "cbOptionView";
            this.cbOptionView.Size = new System.Drawing.Size(121, 24);
            this.cbOptionView.TabIndex = 1;
            this.cbOptionView.SelectedIndexChanged += new System.EventHandler(this.cbOptionView_SelectedIndexChanged);
            // 
            // cbDrive
            // 
            this.cbDrive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDrive.FormattingEnabled = true;
            this.cbDrive.Location = new System.Drawing.Point(12, 12);
            this.cbDrive.Name = "cbDrive";
            this.cbDrive.Size = new System.Drawing.Size(121, 24);
            this.cbDrive.TabIndex = 0;
            this.cbDrive.SelectedIndexChanged += new System.EventHandler(this.cbDrive_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.wbDrive);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 390);
            this.panel2.TabIndex = 1;
            // 
            // wbDrive
            // 
            this.wbDrive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbDrive.Location = new System.Drawing.Point(0, 0);
            this.wbDrive.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbDrive.Name = "wbDrive";
            this.wbDrive.Size = new System.Drawing.Size(800, 390);
            this.wbDrive.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbOptionView;
        private System.Windows.Forms.ComboBox cbDrive;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.WebBrowser wbDrive;
    }
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace BT1
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, int dx, int dy, uint cButtons, uint dwExtraInfo);
        //Mouse actions
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int x, int y);



        private List<DriveInfo> drives;
        private DriveInfo driveSelected;
        private string optionViewSelected;
        public Form1()
        {
            InitializeComponent();
            cbOptionView.SelectedIndex = 0;
            optionViewSelected = cbOptionView.SelectedItem as string;
            initailData();
        }

        private void initailData()
        {
            loadDrives();
        }

        /// <summary>
        /// Loads all Drives of the Computer and returns a List.
        /// </summary>
        private void loadDrives()
        {
            this.drives = new List<DriveInfo>();
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady)
                {
                    this.drives.Add(drive);
                }
            }
            cbDrive.DataSource = this.drives;
        }

        private void cbDrive_SelectedIndexChanged(object sender, EventArgs e)
        {
            showListView();
        }

        private void cbOptionView_SelectedIndexChanged(object sender, EventArgs e)
        {
            showListView();
        }

        private void showListView()
        {
            driveSelected = cbDrive.SelectedItem as DriveInfo;
            optionViewSelected = cbOptionView.SelectedItem as string;
           if(driveSelected !=null && !string.IsNullOrEmpty(optionViewSelected))
            {
                wbDrive.Url = new Uri(driveSelected.Name);
            }
        }

        private void clicker(int x, int y, int type)
        {
            SetCursorPos(x, y);
            this.Refresh();
            Application.DoEvents();
            if(type == -1)
            {
                mouse_event(MOUSEEVENTF_LEFTDOWN, x, y, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTUP, x, y, 0, 0);
            }
            else
            {
                mouse_event(MOUSEEVENTF_RIGHTDOWN, x, y, 0, 0);
                mouse_event(MOUSEEVENTF_RIGHTUP, x, y, 0, 0);
            }
           
        }

        //private void Form1_MouseDown(object sender, MouseEventArgs e)
        //{

        //    label1.Text = "Local position is " + e.X + ", " + e.Y + ".";
        //    label2.Text = "Global position is " + Cursor.Position.X + ", " + Cursor.Position.Y + ".";
        //}
    }
}

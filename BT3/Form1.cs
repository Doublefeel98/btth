﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BT3
{
    public partial class Form1 : Form
    {
        private DirectoryInfo curDir;

        public Form1()
        {
            InitializeComponent();
        }

        private void loadTreeView()
        {
            // them danh sach cac icon
            treeView1.ImageList = new ImageList();
            string requiredPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            treeView1.ImageList.Images.Add(new Icon(requiredPath + "\\Icons\\mycomputer.ico"));
            treeView1.ImageList.Images.Add(new Icon(requiredPath + "\\Icons\\drive.ico"));
            treeView1.ImageList.Images.Add(new Icon(requiredPath + "\\Icons\\folder_close.ico"));
            treeView1.ImageList.Images.Add(new Icon(requiredPath + "\\Icons\\folder_open.ico"));
            treeView1.ImageList.Images.Add(new Icon(requiredPath + "\\Icons\\document.ico"));

            // them nut My computer va cac o dia
            TreeNode thisPC = new TreeNode("This PC");
            thisPC.Tag = "This PC";
            thisPC.ImageIndex = 0;
            thisPC.SelectedImageIndex = 0;
            treeView1.Nodes.Add(thisPC);
            // them cac node o dia vao mycomputer node
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                TreeNode driveNode = new TreeNode(drive.Name);
                driveNode.Tag = drive.RootDirectory;
                driveNode.ImageIndex = 1;
                driveNode.SelectedImageIndex = 1;
                thisPC.Nodes.Add(driveNode);
                if(drive.RootDirectory.GetDirectories().Length > 0)
                {
                    TreeNode tempNode = new TreeNode("temp");
                    driveNode.Nodes.Add(tempNode);
                }
            }
            thisPC.Expand();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadTreeView();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // lay node dang duoc chon
            TreeNode selectedNode = treeView1.SelectedNode;

            try
            {
                if (selectedNode.Tag.GetType() == typeof(DirectoryInfo))
                {
                    // xoa danh sach node da co
                    selectedNode.Nodes.Clear();
                    // them cac node thu muc con
                    DirectoryInfo dir = (DirectoryInfo)selectedNode.Tag;
                    foreach (DirectoryInfo subDir in dir.GetDirectories())
                    {
                        TreeNode dirNode = new TreeNode(subDir.Name);
                        dirNode.Tag = subDir;
                        dirNode.ImageIndex = 2;
                        dirNode.SelectedImageIndex = 3;
                        selectedNode.Nodes.Add(dirNode);
                        if(subDir.GetDirectories().Length > 0)
                        {
                            TreeNode tempNode = new TreeNode("temp");
                            dirNode.Nodes.Add(tempNode);
                        }
                    }
                    // hien thi ben listview
                    curDir = dir;
                    openDirectory();
                }
                // mo rong node
                selectedNode.Expand();
            }
            catch
            {
            }
        }

        private void openDirectory()
        {
            textBox1.Text = curDir.FullName;
            webBrowser1.Url = new Uri(curDir.FullName);
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            // lay node dang duoc chon
            TreeNode selectedNode = e.Node;

            try
            {
                if (selectedNode.Tag.GetType() == typeof(DirectoryInfo))
                {
                    // xoa danh sach node da co
                    selectedNode.Nodes.Clear();
                    // them cac node thu muc con
                    DirectoryInfo dir = (DirectoryInfo)selectedNode.Tag;
                    foreach (DirectoryInfo subDir in dir.GetDirectories())
                    {
                        TreeNode dirNode = new TreeNode(subDir.Name);
                        dirNode.Tag = subDir;
                        dirNode.ImageIndex = 2;
                        dirNode.SelectedImageIndex = 3;
                        selectedNode.Nodes.Add(dirNode);
                        if (subDir.GetDirectories().Length > 0)
                        {
                            TreeNode tempNode = new TreeNode("temp");
                            dirNode.Nodes.Add(tempNode);
                        }
                    }
                    // hien thi ben listview
                    curDir = dir;
                    openDirectory();
                }
                // mo rong node
                selectedNode.Expand();
            }
            catch
            {
            }
        }
    }
}
